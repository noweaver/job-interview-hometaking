--Task 04: Your next task is something for the upper management. Since they need to know the overall performance of our company.
--Provide a KPI report containing the yearly figures for bookings, the average driver evaulation and revenue for 2016.
--Print those results to the log as well.


select * from booking;

select count(*) from booking
where
  start_date >= to_date('20160101', 'yyyymmdd')
  and
  end_date <= to_date('20161231', 'yyyymmdd')
;

select id_driver, round(avg(rating),1) as avg_rating, round(avg(tour_value),1) as avg_tour_value
from booking
where
  start_date >= to_date('20160101', 'yyyymmdd')
  and
  end_date <= to_date('20161231', 'yyyymmdd')
group by
  id_driver
order by
  avg_rating desc, avg_tour_value desc
;

select b.id_driver, d.name, b.avg_rating, b.avg_tour_value
from
  (
  select id_driver, round(avg(rating),1) as avg_rating, round(avg(tour_value),1) as avg_tour_value
  from booking
  where
    start_date >= to_date('20160101', 'yyyymmdd')
    and
    end_date <= to_date('20161231', 'yyyymmdd')
  group by
    id_driver
  ) as b,
  driver d
where
  d.id = b.id_driver
order by
  avg_rating desc, avg_tour_value desc
;

select * from booking
where
  start_date >= to_date('20160101', 'yyyymmdd')



