-- Task 03: We do care a lot about relationships between our drivers and passengers.
-- Our goal is to create a very positive user experience for both, thus we promote passengers to request their favorite drivers frequently.
-- In order to know how successful we are at creating a good passenger driver relationship, please provide a list of the
-- TOP 10 strongest relationships between passengers and drivers. This relationship is defined by the number of tours they did together.

select * from booking;

select id_driver, id_passenger
from booking
--group by id_driver
order by id_driver, id_passenger desc
;

select tmp.ranking, tmp.id_driver, tmp.id_passenger, tmp.tour_value 
from 
(
	select id_driver, id_passenger, tour_value, row_number() over (partition by id_driver order by tour_value desc) as ranking from booking
) as tmp
where
	tmp.ranking < 11
;

select count(*)
from
  (
    SELECT
      id_driver,
      id_passenger,
      tour_value,
      row_number()
      OVER (
        PARTITION BY id_driver
        ORDER BY tour_value DESC ) AS ranking
    FROM booking
  ) as b
;

select count(*)
from
  (SELECT
     tmp.ranking,
     tmp.id_driver,
     tmp.id_passenger,
     tmp.tour_value
   FROM
     (
       SELECT
         id_driver,
         id_passenger,
         tour_value,
         row_number()
         OVER (
           PARTITION BY id_driver
           ORDER BY tour_value DESC ) AS ranking
       FROM booking
     ) AS tmp
   WHERE
     tmp.ranking < 11
  ) as b
;

select b.ranking, b.id_driver as driver_id, d.name as driver_name, b.id_passenger as passenger_id, p.name as passenger_name, b.tour_values
from
  (
    select tmp.ranking as ranking, tmp.id_driver as id_driver, tmp.id_passenger as id_passenger, tmp.tour_value as tour_values
    from
    (
    select id_driver, id_passenger, tour_value, row_number() over (partition by id_driver order by tour_value desc) as ranking from booking
    ) as tmp
    where
      tmp.ranking < 11
    ) as b,
  driver d,
  passenger p
where
  d.id = b.id_driver and p.id = b.id_passenger
;
