--Task 02: Based on your new database, create a report listing the TOP 10 drivers from 2016.
--We like drivers with a high tour value and a good average rating.
--For now you can just print the result as a nice list to your log.

select * from booking;

select id_driver, round(avg(tour_value),1) from booking
group by id_driver
order by id_driver
;

select * from driver;

select id_driver, sum(tour_value) as high_tour_value
from booking
group by id_driver
order by
	high_tour_value desc
;

select d.id, d."name", b.high_tour_value
from
	driver as d,
	(select id_driver, sum(tour_value) as high_tour_value
	 from booking
	 group by id_driver) as b
where
	d.id = b.id_driver
order by
	high_tour_value desc
;

select d.id, d."name", b.high_tour_value, b.rating_avg
from
	driver as d,
	(select id_driver, sum(tour_value) as high_tour_value, round(avg(rating), 1) as rating_avg
	 from booking
	 group by id_driver) as b
where
	d.id = b.id_driver
order by
	high_tour_value desc, rating_avg desc
limit 10
;


select * from booking, driver;