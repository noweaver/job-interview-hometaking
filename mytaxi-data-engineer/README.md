## Dear Manager & Chief Engineer

Hello, I am Ryan Geonguk Ahn. 

I have implemented all the requirements. However, some conditions are ambiguous. For example, I think it is a little bit confusing for Task1 to prioritize which items to make first. The revenue part of Task2 is confused, too. :) Therefore, the tour_value has been solved. I think it is a double meaning. 

Also, I honestly didn't hand-on for over a year. So, it took a long time to implement the code. Also, I didn't have the Spark, the Kafka, and PostgreSQL on my MacBook.

So, I tried with the Docker first time, but it took a lot of time to set up something. Maybe, I think configuration file is wrong such as network setting. So I just installed and proceeded using the binary files from internet.

Finally, I show you this project following as;

1. I have implemented on each Task.java for each task. For example, Task1.java indicate task01 requirement. Application.java can also execute all tasks.

1. AppWriter.java has two functions. One is that the logs of each task are stored in each task.csv by AppWriter invoking Reporter.java. The other feature is to forward the log to each Task topic in Kafka.

1. Since the implementation of an index is more comfortable to process in SQL, I have traversed the data with PostgreSQL that complies with ANSI and then processed this SQL with SparkSQL.

I hope I put the configuration file in the / conf directory, but I have not implemented it because it is not a core issue. Instead, most of the configuration information was in Configure.java and Info.java.

```bash
.
├── DataEngineerTestApp.iml
├── README.md
├── pom.xml
├── sql
│   ├── task2).sql
│   ├── task3).sql
│   └── task4).sql
└── src
    └── main
        ├── java
        │   ├── com
        │   │   └── mytaxi
        │   │       └── dataengineertestapp
        │   │           ├── AppWriter.java
        │   │           ├── Application.java
        │   │           ├── Task1.java
        │   │           ├── Task2.java
        │   │           ├── Task3.java
        │   │           ├── Task4.java
        │   │           ├── com
        │   │           │   └── mytaxi
        │   │           │       └── dataengineertestapp
        │   │           │           └── utils
        │   │           │               ├── Configure.java
        │   │           │               ├── DataSetResult.java
        │   │           │               ├── Info.java
        │   │           │               └── SparkUtils.java
        │   │           ├── kafka
        │   │           │   ├── IProducer.java
        │   │           │   └── TaskProducer.java
        │   │           ├── postgres
        │   │           │   ├── DbManager.java
        │   │           │   ├── IDataLoader.java
        │   │           └── report
        │   │               └── Reporter.java
        │   └── resources
        │       ├── booking.csv
        │       ├── driver.csv
        │       └── passenger.csv
        └── resource
            └── logback.xml

```

**Data Engineer - Test Application:**

We as a data team want to gather data from various sources in our company, enrich the collected data and deliver it to our stakeholders.  
During this test you will work a bit on all of these steps. Please follow the tasks in the given order and use the technologies listed below.  
You may use any additional library that might help you and version of the technologies below, but we recommend sticking to the most recent one.  


**Requirements:**
* Programing language: Java, Python
* Processing framework: Spark
* Queuing technology: Kafka
* Database technology: Postgres.

In case you decided to go for Python, please use the given Interfaces just as a hint on how to get started.

**Database tables:**  
`driver (id BIGINT, date_created TIMESTAMP, name CHARACTER VARYING(255))`  
`passenger (id BIGINT, date_created TIMESTAMP, name CHARACTER VARYING(255))`  
`booking ( id BIGINT, date_created TIMESTAMP, id_driver BIGINT, id_passenger BIGINT, rating INT, start_date TIMESTAMP, end_date TIMESTAMP,  tour_value BIGINT)`  

**Tasks:**
* Task 01:
    Given the DDL above, please create a full database model including keys, constraints or any other requirement necessary.  
    Upload the csv files to your local postgres database.
* Task 02:
    Based on your new database, create a report listing the TOP 10 drivers from 2016.  
    We like drivers with a high tour value and a good average rating.  
    For now you can just print the result as a nice list to your log.  
* Task 03:
    We do care a lot about relationships between our drivers and passengers.  
    Our goal is to create a very positive user experience for both, thus we promote passengers to request their favorite drivers frequently.  
    In order to know how successful we are at creating a good passenger driver relationship, please provide a list of the   
    TOP 10 strongest relationships between passengers and drivers. This relationship is defined by the number of tours they  
    did together.
* Task 04:
    Your next task is something for the upper management. Since they need to know the overall performance of our company.  
    Provide a KPI report containing the yearly figures for bookings, the average driver evaulation and revenue for 2016.  
    Print those results to the log as well.  
* Task 05:
    The last task is to deliver these KPIs to our Management Reporting Tool.  
    This tool can fetch data from our Kafka Queue. Thus setup your own Kafka Broker locally and publish the results of Task 02, Task 03 and Task 04   
    to different kafka topic.
   
    
