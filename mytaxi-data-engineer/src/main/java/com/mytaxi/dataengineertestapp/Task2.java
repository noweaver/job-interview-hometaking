package com.mytaxi.dataengineertestapp;

import com.mytaxi.dataengineertestapp.kafka.TaskProducer;
import com.mytaxi.dataengineertestapp.utils.Configure;
import com.mytaxi.dataengineertestapp.utils.Info;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class Task2 {
    private static Logger logger = LoggerFactory.getLogger(Task2.class);

    public static void main(String[] args) {
        try {
            new Task2().execute();
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
    }

    public void execute() throws ClassNotFoundException {
        SQLContext sqlContext = Configure.getInstance().configureSpark();
        String url = Configure.getInstance().configureDb();

        Dataset<Row> booking = sqlContext.jdbc(url, "booking");
        Dataset<Row> driver = sqlContext.jdbc(url, "driver");

        try {
            booking.createGlobalTempView("booking");
            driver.createGlobalTempView("driver");

            Dataset<Row> dataset = sqlContext.sql(
                    "select d.id, d.name, b.high_tour_value, b.rating_avg " +
                            "from " +
                            "   (select id_driver, sum(tour_value) as high_tour_value, round(avg(rating), 1) as rating_avg " +
                            "   from global_temp.booking " +
                            "   group by id_driver) as b, " +
                            "   global_temp.driver as d " +
                            "where b.id_driver = d.id " +
                            "order by high_tour_value desc, rating_avg desc " +
                            "limit 10");

            dataset.show();

            TaskProducer taskProducer = new TaskProducer(Configure.getInstance().configureKafka());
            AppWriter.getInstance().toKafka(taskProducer, dataset, Info.TASK2_TOPIC);
            AppWriter.getInstance().createReport(dataset, Info.REPORT_PATH + "Task2");

            System.out.println();
        } catch (AnalysisException e) {
            logger.error(e.getMessage());
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        } catch (ExecutionException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            Configure.getInstance().closeSparkContext();
        }
    }
}
