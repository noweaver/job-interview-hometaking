package com.mytaxi.dataengineertestapp.utils;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public enum Configure {
    INSTANCE;

    private static Logger logger = LoggerFactory.getLogger(Configure.class);
    static String test = "";

    public static Configure getInstance() {
        test = "test";
        return INSTANCE;
    }

    public SQLContext configureSpark() {
        SparkConf conf = new SparkConf()
                .setAppName("Java Spark Application")
                .set("spark.driver.allowMultipleContexts", "true")
                .setMaster("local[*]");

        JavaSparkContext sc = new JavaSparkContext(conf);
        SparkSession spark = SparkSession
                .builder()
                .appName("Java Spark SQL basic example")
                .config("spark.some.config.option", "some-value")
                .getOrCreate();

        SQLContext sqlContext = new SQLContext(sc);
        return sqlContext;
    }

    public String configureDb() throws ClassNotFoundException {
        String host = "localhost";
        String database = "postgres";

        Class.forName("org.postgresql.Driver");
        String dbUrl = String.format("jdbc:postgresql://%s/%s", host, database);

        return dbUrl;
    }

    public Properties configureKafka() {
        Properties configs = new Properties();
        configs.put("bootstrap.servers", Info.KAFKA_ENDPOINT);
        // configs.put("acks", "all");
        // configs.put("block.on.buffer.full", "true");
        configs.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        configs.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        return configs;
    }

    public void closeSparkContext() {
        SQLContext sqlContext = this.configureSpark();
        sqlContext.clearCache();
        sqlContext.sparkSession().close();
    }
}