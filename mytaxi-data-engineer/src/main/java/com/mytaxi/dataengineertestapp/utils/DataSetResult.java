package com.mytaxi.dataengineertestapp.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class DataSetResult {
    private List<String> columnNames = new ArrayList<>();
    private List<List<Object>> rows = new ArrayList<>();

    public List<String> getColumnNames() {
        return columnNames;
    }
    public List<List<Object>> getRows() {
        return rows;
    }

    public String mkString() {
        StringBuffer sb = new StringBuffer(1024);
        sb.append(StringUtils.join(columnNames, ",")).append("\n");
        for (List<Object> row : rows) {
            sb.append(StringUtils.join(row, ",")).append("\n");
        }

        String returnStr = sb.toString();
        int returnStrLength = returnStr.length();

        return returnStr.substring(0, returnStrLength-1);
    }
}
