package com.mytaxi.dataengineertestapp.utils;

public class Info {
    public static final String DRIVER_CSV = "src/main/java/resources/driver.csv";
    public static final String PASSENGER_CSV = "src/main/java/resources/passenger.csv";
    public static final String BOOKING_CSV = "src/main/java/resources/booking.csv";

    public static final String POSTGRESQL_USERNAME = "ryan";
    public static final String POSTGRESQL_PASSWORD = "asdf1234";

    public static final String REPORT_PATH = "/Volumes/Workspace/github/data-engineer-application/report/";

    // KAFKA Configuring
    public static final String KAFKA_ENDPOINT = "localhost:9092"; //bootstrap.server
    public static final String TASK2_TOPIC = "task2";
    public static final String TASK3_TOPIC = "task3";
    public static final String TASK4_TOPIC = "task4";

}