package com.mytaxi.dataengineertestapp;

import com.mytaxi.dataengineertestapp.kafka.TaskProducer;
import com.mytaxi.dataengineertestapp.utils.Configure;
import com.mytaxi.dataengineertestapp.utils.Info;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class Task3 {
    private static Logger logger = LoggerFactory.getLogger(Task3.class);

    public static void main(String[] args) {
        try {
            new Task3().execute();
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
    }

    public void execute() throws ClassNotFoundException {
        SQLContext sqlContext = Configure.getInstance().configureSpark();
        String url = Configure.getInstance().configureDb();

        Dataset<Row> booking = sqlContext.jdbc(url, "booking");
        Dataset<Row> driver = sqlContext.jdbc(url, "driver");
        Dataset<Row> passenger = sqlContext.jdbc(url, "passenger");

        try {
            booking.createGlobalTempView("booking");
            driver.createGlobalTempView("driver");
            driver.createGlobalTempView("passenger");

            String sql =
                "select b.ranking, b.id_driver, b.id_passenger, b.tour_value " +
                "from " +
                "   ( " +
                "   select " +
                "      temp.id_driver, " +
                "      temp.id_passenger, " +
                "      temp.tour_value, " +
                "      row_number() " +
                "      over (partition by temp.id_driver " +
                "             order by temp.tour_value desc) as ranking " +
                "   from " +
                "      global_temp.booking as temp " +
                "   ) as b " +
                "where b.ranking < 11";

            logger.info(sql);
            Dataset<Row> dataset = sqlContext.sql(sql);

            dataset.show();

            TaskProducer taskProducer = new TaskProducer(Configure.getInstance().configureKafka());
            AppWriter.getInstance().toKafka(taskProducer, dataset, Info.TASK3_TOPIC);
            AppWriter.getInstance().createReport(dataset, Info.REPORT_PATH + "Task3");

            System.out.println();
        } catch (AnalysisException e) {
            logger.error(e.getMessage());
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        } catch (ExecutionException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            Configure.getInstance().closeSparkContext();
        }
    }
}
