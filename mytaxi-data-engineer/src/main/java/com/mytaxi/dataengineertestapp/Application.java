package com.mytaxi.dataengineertestapp;

import java.sql.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {
    private static Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        try {
            new Task1().execute();
            new Task2().execute();
            new Task3().execute();
            new Task4().execute();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
