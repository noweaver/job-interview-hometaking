package com.mytaxi.dataengineertestapp;

import com.mytaxi.dataengineertestapp.utils.DataSetResult;
import com.mytaxi.dataengineertestapp.utils.SparkUtils;
import com.mytaxi.dataengineertestapp.kafka.IProducer;
import com.mytaxi.dataengineertestapp.report.Reporter;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

public enum  AppWriter {
    INSTANCE;

    private static Logger logger = LoggerFactory.getLogger(AppWriter.class);
    static String test = "";
    private String columnHeader;

    public static AppWriter getInstance() {
        test = "test";
        return INSTANCE;
    }


    public void toKafka(IProducer producer, Dataset<Row> dataset, String topic) throws ExecutionException, InterruptedException {
        DataSetResult dr = SparkUtils.getDataSetResult(dataset);
        getColumnHeader(dr.getColumnNames());

        // kafka processing
        producer.send(topic, dr.mkString());
    }

    public void setColumnHeader(String columnHeader) {
        this.columnHeader = columnHeader;
    }

    private void getColumnHeader(List<String> columnNames) {
        StringBuffer sb = new StringBuffer(256);
        for (String column : columnNames) {
            sb.append(column + ",");
        }

        setColumnHeader(sb.substring(0, sb.length() - 1).toString());
    }

    public void createReport(Dataset<Row> dataset, String filePath) throws IOException {
        Reporter.delete(filePath);
        dataset.write().csv(filePath);

        Reporter.writeReport(filePath, this.columnHeader);
        Reporter.delete(filePath);
    }
}
