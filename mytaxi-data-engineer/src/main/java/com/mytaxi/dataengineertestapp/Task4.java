package com.mytaxi.dataengineertestapp;

import com.mytaxi.dataengineertestapp.kafka.TaskProducer;
import com.mytaxi.dataengineertestapp.utils.Configure;
import com.mytaxi.dataengineertestapp.utils.Info;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class Task4 {
    private static Logger logger = LoggerFactory.getLogger(Task4.class);

    public static void main(String[] args) {
        try {
            new Task4().execute();
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
    }

    public void execute() throws ClassNotFoundException {
        SQLContext sqlContext = Configure.getInstance().configureSpark();
        String url = Configure.getInstance().configureDb();

        Dataset<Row> booking = sqlContext.jdbc(url, "booking")
                .select("*")
                .where("start_date >= '2016-01-01' and end_date <= '2016-12-31'");

        Dataset<Row> driver = sqlContext.jdbc(url, "driver");

        try {
            booking.createGlobalTempView("booking");
            driver.createGlobalTempView("driver");

            String sql =
                    "select " +
                    "   b.id_driver, " +
                    "   d.name, " +
                    "   b.avg_rating, " +
                    "   b.avg_tour_value " +
                    "from " +
                    "   (" +
                    "       select " +
                    "           id_driver, " +
                    "           round(avg(rating), 1) as avg_rating, " +
                    "           round(avg(tour_value), 1) as avg_tour_value " +
                    "       from " +
                    "           global_temp.booking " +
                    "       group by " +
                    "           id_driver " +
                    "   ) as b, " +
                    "   global_temp.driver as d " +
                    "where " +
                    "   d.id = b.id_driver " +
                    "order by " +
                    "   avg_rating desc, " +
                    "   avg_tour_value desc";

            logger.info(sql);
            Dataset<Row> dataset = sqlContext.sql(sql);

            dataset.show();

            TaskProducer taskProducer = new TaskProducer(Configure.getInstance().configureKafka());
            AppWriter.getInstance().toKafka(taskProducer, dataset, Info.TASK4_TOPIC);
            AppWriter.getInstance().createReport(dataset, Info.REPORT_PATH + "Task4");

            System.out.println();
        } catch (AnalysisException e) {
            logger.error(e.getMessage());
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        } catch (ExecutionException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            Configure.getInstance().closeSparkContext();
        }
    }
}
