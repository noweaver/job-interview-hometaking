package com.mytaxi.dataengineertestapp.kafka;

import java.util.concurrent.ExecutionException;

public interface IProducer
{
    /**
     * Send playload to queue
     *
     * @param topic    : Topic in a queue
     * @param payload: Data to publish
     */
    void send(String topic, Object payload) throws ExecutionException, InterruptedException;
}
