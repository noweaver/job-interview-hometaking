package com.mytaxi.dataengineertestapp.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class TaskProducer implements IProducer {
    private static Logger logger = LoggerFactory.getLogger(TaskProducer.class);

    private final Properties kafkaConfigs;
    KafkaProducer<String, String> producer;

    public TaskProducer(Properties kafkaConfigs) {
        this.kafkaConfigs = kafkaConfigs;
        this.producer = new KafkaProducer<>(kafkaConfigs);
    }

    @Override
    public void send(String topic, Object payload) throws ExecutionException, InterruptedException {
        Future<RecordMetadata> future = this.producer.send(new ProducerRecord<>(topic, payload.toString()),
                (metadata, exception) -> {
                    if (metadata != null) {
                        logger.info("Message produced, offset: " + metadata.offset());
                        logger.info("Message produced, partition : " + metadata.partition());
                        logger.info("Message produced, topic: " + metadata.topic());
                    } else {
                        logger.error(exception.getMessage());
                        exception.printStackTrace();
                    }
                });

        this.producer.flush();
        this.producer.close();
    }
}
