package com.mytaxi.dataengineertestapp;

import com.mytaxi.dataengineertestapp.postgres.DbManager;
import com.mytaxi.dataengineertestapp.utils.Configure;
import com.mytaxi.dataengineertestapp.utils.Info;
import org.apache.spark.sql.SQLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

public class Task1 {
    private static Logger logger = LoggerFactory.getLogger(Task1.class);

    public static void main(String[] args) {
        try {

            new Task1().execute();

        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    public void execute() throws ClassNotFoundException, SQLException {
        DbManager dbManager = new DbManager();
        String url = Configure.getInstance().configureDb();
        SQLContext sqlContext = Configure.getInstance().configureSpark();

        // driver.csv
        dbManager.toDb(sqlContext, url, "driver", Info.DRIVER_CSV);
        // passenger.csv
        dbManager.toDb(sqlContext, url, "passenger", Info.PASSENGER_CSV);
        // booking.csv
        dbManager.toDb(sqlContext, url, "booking", Info.BOOKING_CSV);

        Configure.getInstance().closeSparkContext();
    }
}
