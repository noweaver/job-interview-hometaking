package com.mytaxi.dataengineertestapp.postgres;

import com.mytaxi.dataengineertestapp.utils.Configure;
import com.mytaxi.dataengineertestapp.utils.Info;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DbManager implements IDataLoader {
    private static Logger logger = LoggerFactory.getLogger(DbManager.class);

    private Properties properties;
    private String tableName;
    private Connection dbConnection;
    private boolean isDrop = false;
    private boolean isCreated = false;

    public DbManager() throws SQLException, ClassNotFoundException {
        this.properties = new Properties();
        this.properties.setProperty("user", Info.POSTGRESQL_USERNAME);
        this.properties.setProperty("password", Info.POSTGRESQL_PASSWORD);
        // this.properties.setProperty("ssl", "true");

        initiailize();
    }

    private void initiailize() throws ClassNotFoundException, SQLException {
        this.dbConnection = DriverManager.getConnection(Configure.getInstance().configureDb(), properties);

        dropTable("booking").dropTable("driver").dropTable("passenger");
        createTable("driver").createTable("passenger").createTable("booking");

        if (!isDropTable() && !isExistTable()) {
            logger.error("IS NOT DROP TABLE OR IS NOT CREATE TABLE");
            System.exit(0);
        }
    }

    @Override
    public void toDb(SQLContext sqlContext, String connectionUrl, String tableName, String filePath) {
        String[] columns = getColumns(filePath);

        // get schema
        StructType schema = sqlContext.read().jdbc(connectionUrl, tableName, properties).schema();

        Dataset<Row> df = sqlContext.read().schema(schema).option("timestampFormat", "yyyy-MM-dd HH:mm").csv(filePath);
        Dataset<Row> columnDf = df.toDF(columns);
        columnDf.show();

        // Insert into db;
        columnDf.write().mode("append").jdbc(connectionUrl, tableName, properties);

        Dataset<Row> dbDf = sqlContext.jdbc(connectionUrl, tableName);
        dbDf.show();

        if (logger.isDebugEnabled()) {
            long beforeRowSize = columnDf.count();
            long afterRowSize = dbDf.count();

            if (beforeRowSize == afterRowSize) {
                logger.info("The " + tableName + " table row count => " + afterRowSize);
            } else {
                logger.info("Before insert to " + tableName + "'s Row count => " + beforeRowSize);
                logger.info("After insert to " + tableName + "'s Row count => " + afterRowSize);
            }
        }

    }

    private String[] getColumns(String filePath) {
        String[] split = filePath.split("/");
        String fileName = split[split.length - 1].split(".csv")[0];
        System.out.println(fileName);

        String[] columns = null;
        if (fileName.equalsIgnoreCase("booking")) {
            columns = new String[]{"id", "date_created", "id_driver", "id_passenger", "rating", "start_date", "end_date", "tour_value"};
        } else if (fileName.equalsIgnoreCase("driver")) {
            columns = new String[]{"id", "date_created", "name"};
        } else if (fileName.equalsIgnoreCase("passenger")) {
            columns = new String[]{"id", "date_created", "name"};
        }

        return columns;
    }

    public DbManager dropTable(String tableName) {
        try {
            if (null != dbConnection) {
                logger.info("Successfully dropped dbConnection to database.");

                Statement statement = dbConnection.createStatement();

                String dropSql = "DROP TABLE IF EXISTS " + tableName + ";";
                statement.execute(dropSql);

                logger.info("Finished dropping table (if existed) " + tableName);
                this.isDrop = true;
            } else {
                logger.info("DB Connection is NOT Connection");
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }

        return this;
    }

    public boolean isDropTable() {
        return this.isDrop;
    }

    public boolean isExistTable() {
        return this.isCreated;
    }

    public DbManager createTable(String tableName) {
        try {
            if (null != dbConnection) {
                logger.info("Successfully created dbConnection to database.");
                Statement statement = dbConnection.createStatement();

                String createTableSql = null;
                if ("booking".equalsIgnoreCase(tableName)) {
                    createTableSql =
                            "CREATE TABLE IF NOT EXISTS booking (" +
                                    "id BIGINT PRIMARY KEY NOT NULL, " +
                                    "date_created TIMESTAMP, " +
                                    "id_driver BIGINT REFERENCES driver(id), " +
                                    "id_passenger BIGINT REFERENCES passenger(id), " +
                                    "rating INT, " +
                                    "start_date TIMESTAMP, " +
                                    "end_date TIMESTAMP, " +
                                    "tour_value BIGINT);";

                } else if ("driver".equalsIgnoreCase(tableName)) {
                    createTableSql =
                            "CREATE TABLE IF NOT EXISTS driver (" +
                                    "id BIGINT PRIMARY KEY NOT NULL, " +
                                    "date_created TIMESTAMP, " +
                                    "name CHARACTER VARYING(255));";

                } else if ("passenger".equalsIgnoreCase(tableName)) {
                    createTableSql =
                            "CREATE TABLE IF NOT EXISTS passenger (" +
                                    "id BIGINT PRIMARY KEY NOT NULL, " +
                                    "date_created TIMESTAMP, " +
                                    "name CHARACTER VARYING(255));";

                }
                statement.execute(createTableSql);
            }

            this.isCreated = false;
            logger.info("Create Table: " + tableName);
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }

        return this;
    }
}
