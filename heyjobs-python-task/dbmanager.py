#!/usr/bin/env python3
# -*- coding: utf8 -*-


import psycopg2 as pg2


class DbManager:
    def __init__(self, host, port, db_name, user, password):
        self.host = host
        self.port = port
        self.db_name = db_name
        self.user = user
        self.password = password

    def get_connection(self):
        url = 'postgresql://{}:{}@{}:{}/{}'
        url = url.format(self.user, self.password, self.host, self.port, self.db_name)

        try:
            conn = pg2.connect(url, client_encoding='utf8')
            return conn
        except Exception as e:
            print(str(e))

    def drop(self):
        conn = self.get_connection()
        cursor = conn.cursor()

        sql = "drop table if exists test.jobs"
        cursor.execute(sql)
        conn.commit()

    def create_table(self):
        conn = self.get_connection()
        cursor = conn.cursor()

        sql = "create table test.jobs(id varchar, uid varchar, title varchar)"
        cursor.execute(sql)
        conn.commit()

    def insert(self, jobs):
        conn = self.get_connection()
        cursor = conn.cursor()

        sql = "insert into test.jobs(id, uid, title) values(%s, %s, %s)"

        id = 1
        for uid, title in jobs.items():
            cursor.execute(sql, (id, uid, title))
            id += 1

        conn.commit()
        print("Done")