#!/usr/bin/env python3
# -*- coding: utf8 -*-

import requests

from bs4 import BeautifulSoup


class Scraper:
    def __init__(self, url):
        self.url = url

    def read(self):
        if self.url is "":
            return None

        response = requests.get(self.url)
        html = BeautifulSoup(response.content, 'html.parser')

        return html

    def read_from_file(self):
        try:
            with open("temp.html", "r") as file:
                html = file.read()
            return html
        except Exception as e:
            print(str(e))

    def write_to_file(self, html):
        try:
            with open("temp.html", "w") as file:
                file.write(html.prettify())
        except Exception as e:
            print(str(e))


