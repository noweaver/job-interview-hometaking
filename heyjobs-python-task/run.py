#!/usr/bin/env python3
# -*- coding: utf8 -*-


import re
import json
import requests

import psycopg2 as pg2

from bs4 import BeautifulSoup


def get_html(url):
    response = requests.get(url)
    html = BeautifulSoup(response.content, 'html.parser')

    return html


def read_from_file():
    try:
        with open("temp.html", "r") as file:
            html = file.read()
        return html
    except Exception as e:
        print(str(e))


def write_to_file(html):
    try:
        with open("temp.html", "w") as file:
            file.write(html.prettify())
    except Exception as ex:
        print(str(ex))


def get_json(html_text, to_find):
    json_text = re.findall(to_find, html_text, re.M)[0]
    print(json_text)

    return json.loads(json_text, encoding="utf8")


def get_jobs(jobs):
    jobs_infos = {}
    for job in jobs:
        id = job['id']
        title = job['title']

        jobs_infos[id] = title

    return jobs_infos


def get_connection(host, port, db_name, user, password):
    url = 'postgresql://{}:{}@{}:{}/{}'
    url = url.format(user, password, host, port, db_name)
    print(url)

    try:
        conn = pg2.connect(url, client_encoding='utf8')
        return conn
    except Exception as e:
        print(str(e))


def drop(conn):
    cursor = conn.cursor()

    sql = "drop table if exists test.jobs"
    cursor.execute(sql)
    conn.commit()


def create_table(conn):
    cursor = conn.cursor()

    sql = "create table test.jobs(id varchar, uid varchar, title varchar)"
    cursor.execute(sql)
    conn.commit()


def insert(conn, jobs):
    cursor = conn.cursor()

    sql = "insert into test.jobs(id, uid, title) values(%s, %s, %s)"
    id = 1
    for uid, title in jobs.items():
        cursor.execute(sql, (id, uid, title))
        id += 1

    conn.commit()
    print("Done")


def create_schema(conn):
    cursor = conn.cursor()

    sql = "create schema if not exists test authorization test"
    cursor.execute(sql)
    conn.commit()


if __name__ == '__main__':
    print("Hello scraper!")

    try:
        # get the data from url
        url = "https://www.heyjobs.de/en/jobs-in-berlin"
        html = get_html(url)
        write_to_file(html)
        html_text = read_from_file()

        json_obj = get_json(html_text, "window.__data=(.*?);\s*$")
        jobs_obj = json_obj['jobSearch']['jobs']

        jobs = get_jobs(jobs_obj)
        for key, value in jobs.items():
            print(key + " ==> " + value)

        # DB connection
        db_name = "heyjobs"
        host = "db"
        # host = "localhost"
        port = 5432
        user = "test"
        password = "testpass"

        conn = get_connection(host, port, db_name, user, password)

        create_schema(conn)
        drop(conn)
        create_table(conn)
        insert(conn, jobs)

    except Exception as ex:
        print(str(ex))
